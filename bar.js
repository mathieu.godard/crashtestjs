import { sum, divide, multiply } from './foo.js'

// Test de chaque fonction
const a = 3;
const b = 8;
console.log(`${a} + ${b} = ${sum(a, b)}`);
console.log(`${a} / ${b} = ${divide(a, b)}`);
console.log(`${a} * ${b} = ${multiply(a, b)}`);

// Test d'une opération qui utilise plusieurs fonctions
console.log("(6 + 2) * 4 / 2 = " + divide(multiply(sum(6, 2), 4), 2));