export const sum = function (...numbers) {
    let result = 0;
    for (let i = 0; i < numbers.length; i++) {
        result += numbers[i];
    }
    return result;
}

export const divide = function(a, b) {
    return a / b;
}

export const multiply = function(a, b) {
    return a * b;
}